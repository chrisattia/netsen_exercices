﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace App_Netsen
{
    class NotesModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string ReadingFile { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }


        string _fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "notes.txt");

        public NotesModel()
        {

            SaveCommand = new Command<string>(SaveFile);
            DeleteCommand = new Command(DeleteFile);
            
            
            if (File.Exists(_fileName))
            {
                ReadingFile = File.ReadAllText(_fileName);
                OnPropertyChanged("ReadingFile");
            }
            
        }
       
        async void DeleteFile()
        {
            var res = await App.Current.MainPage.DisplayAlert("Attention", "Êtes vous sur de vouloir supprimer cette note ? ", "Oui je suis sûr", "Non je ne préfère pas");
            if (res)
            {
                if (File.Exists(_fileName))
                {
                    File.Delete(_fileName);
                }
                ReadingFile = string.Empty;
                OnPropertyChanged("ReadingFile");
            }

        }

        void SaveFile(string value)
        {
            
            File.WriteAllText(_fileName, value);
            
        }


        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
