**Netsen exercices**
Pour l'exercice 1 :
Version dotnet utilisée : 3.1.201 
Executer les commandes suivantes :
	dotnet add package Microsoft.EntityFrameworkCore.Sqlite
	dotnet tool install --global dotnet-ef
	dotnet add package Microsoft.EntityFrameworkCore.Design
	dotnet ef migrations add InitialCreate
	dotnet ef database update
	dotnet run
Afin de tester l'ajout, mettre en commentaire les lignes 64,65,66 
et à l'aide d'un logiciel (DB Brower Sqlite) vous pouvez visualiser le fichier 'blogging.db'

Pour l'exercice 2 : 
J'ai utilisé l'émulateur Pixel 2 
Tester l'application : 
Il s'agit ici d'écrire ce que l'on veut sur la note
De l'enregistrer
Si l'on quitte l'application et que l'on revient, le contenu de la note ne sera pas effacé 
Si l'on appuie sur Delete, la note sera une fenêtre s'affichera en demandant si l'on est sur de bien supprimer cette note