﻿using NetsenExercise_One.Entities;
using System;
using System.Linq;

namespace NetsenExercise_One
{
    class Program
    {
        static void Main()
        {
            using (var db = new BloggingContext())
            {
                try
                {
                    // Create
                    Console.WriteLine("Inserting a new blog");
                    db.Add(new Blog { Url = "http://blogs.msdn.com/adonet" });

                    db.SaveChanges();

                    // Read
                    Console.WriteLine("Querying for a blog");
                    var blog = db.Blogs
                        .OrderBy(b => b.BlogId)
                        .First();

                    // Update
                    Console.WriteLine("Updating the blog and adding a post which have one comment and its author");

                    blog.Url = "https://devblogs.microsoft.com/dotnet";
                    Author MyAuthor = new Author
                    {
                        Age = 15,
                        MailAdress = "myadresse@gmail.com",
                        Name = "Dupont ",
                        Password = "root",
                        PathPicture = "Path/To/Picture",               
                        Surname = "Dupond"
                    };
                    Post MyPost = new Post
                    {
                        Title = "Mon premier Poste",
                        Content = "Premier Poste "
                    };
                    Post MyPoste = new Post
                    {
                        Title = "Mon second Poste",

                        Content = "J'ai deux commentaires"
                    };
                    Comment MyComment = new Comment
                    {
                        
                        Content = "Mon premier commentaire"
                    };
                    Comment MyCommente = new Comment
                    {
                        
                        Content = "mon secondCommentaire"
                        
                    };

                    MyAuthor.Posts.Add(MyPoste);
                    MyAuthor.Posts.Add(MyPost);


                    MyAuthor.Comments.Add(MyComment);
                    MyAuthor.Comments.Add(MyCommente);

                    MyPoste.Comments.Add(MyComment);
                    MyPoste.Comments.Add(MyCommente);





                    blog.Authors.Add(MyAuthor);

                    db.SaveChanges();


                    // Delete
                    //Console.WriteLine("Delete the blog");
                    //db.Remove(blog);
                    //db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                
            }
        }
    }
}