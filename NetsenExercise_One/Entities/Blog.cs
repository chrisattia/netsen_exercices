﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetsenExercise_One.Entities
{
    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }
        public List<Author> Authors { get; } = new List<Author>();

    }
}
