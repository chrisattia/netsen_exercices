﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetsenExercise_One.Entities
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public Post Post { get; set; }
        public Author Author { get; set; }



    }
}
