﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetsenExercise_One.Entities
{
        public class Author
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string PathPicture { get; set; }
        public string MailAdress { get; set; }
        public string Password { get; set; }
        public Blog Blog { get; set; }
        public List<Post> Posts { get; } = new List<Post>();
        public List<Comment> Comments { get; } = new List<Comment>();

    }
}
