﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetsenExercise_One.Entities
{
        public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Author Author { get; set; }
        public List<Comment> Comments { get; } = new List<Comment>();

    }
}
